package base;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Base {
	
	public static WebDriver driver; //declare driver as WebDriver object - Class(Global) variable 
	
	@BeforeSuite
	public void setup() {
		ChromeOptions cOption = new ChromeOptions();
		cOption.setHeadless(false);
		
		WebDriverManager.chromedriver().setup();  
		driver = new ChromeDriver(cOption);  //here we use ChromeDriver to instantiate driver variable
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//for all webelements
		}
	
	public void url(String url ) {
		driver.get(url);
	}
	
	public boolean isDisplayed(By locator) {
		return driver.findElement( locator ).isDisplayed();
	}
	
	
	public void click(By locator) {
		//driver.findElement( locator ).click();
		//OR
		WebElement element; //declare a variable named element of WebElement Data Type
		element = driver.findElement(locator);
		element.click();		
	}
	
	
	
	
	public String getCurrentUrl() {
		String url = driver.getCurrentUrl();
		return url;
	}
	
	public String getText(By locator) {
		return driver.findElement(locator).getText();
	}
	
	
	@AfterSuite
	public void tearDown() {
		//driver.quit();
	}
	

}
