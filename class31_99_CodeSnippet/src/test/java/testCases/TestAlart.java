package testCases;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import base.Base;

public class TestAlart extends Base {

	@Test
	public void testAlart() {

		url("https://demoqa.com/alerts");
		click(By.id("timerAlertButton"));

		// Thread.sleep(35000);
		// Q: When do you Thread.sleep?
		// Ans: I do not use Thread.sleep
		// I use webdriver wait to capture any Web Element
		// I use Explicit Wait for any specific Web Element
		// Explicit Wait: for any specific Element, here it is wait for Alert visibility

		WebDriverWait wait;
		wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.alertIsPresent());

		Alert a = driver.switchTo().alert();
		String s = a.getText();
		a.accept();
		System.out.println("Alert timer is : " + s);
		Assert.assertEquals(s, "This alert appeared after 5 seconds");

	}
	@Test
	public void testAlart2() {
		url("https://demoqa.com/alerts");
		click(By.id("confirmButton"));
		Alert a = driver.switchTo().alert();
		String s = a.getText();
		System.out.println(s);
		Assert.assertEquals(s, "Do you confirm action?");
		a.dismiss();	
		
	}
	
	@Test
	public void testAlart3() {
		url("https://demoqa.com/alerts");
		click(By.id("promtButton"));
		
		WebDriverWait wait;
		wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.alertIsPresent());
		
		Alert a = driver.switchTo().alert();
		String s  = a.getText();
		System.out.println(s);
		Assert.assertEquals(s, "Please enter your name");
		//a.sendKeys("apon");
		
		
		
		

	}

}
